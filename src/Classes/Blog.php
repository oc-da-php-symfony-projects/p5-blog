<?php

namespace Blog\Classes;

use Blog\Entity\HomeUser;
use Blog\Orm\MyEntityManager;
use Blog\Router\Router;
use Blog\Service\SendMail;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\HttpFoundation\Session\Storage\PhpBridgeSessionStorage;

class Blog
{
    protected Request $request;
    protected array $config;
    protected MyEntityManager $orm;
    protected Twig $twig;
    protected SendMail $sendMail;
    protected SessionInterface $session;


    public function __construct()
    {
        $this->orm = new MyEntityManager();
        $this->request = Request::createFromGlobals();
        $this->session = $this->session();
        $this->config = (new Config())->siteConfig();
        $this->twig =  new Twig();
        $this->twig->addGlobal('session', $this->session->all());
        $this->sendMail =  new SendMail();
    }


    public function session()
    {
        if ($this->request->hasSession() == false) {
            $session = (new Session(new PhpBridgeSessionStorage()));
            $this->request->setSession($session);
            $session->start();
        }
        if (empty($this->request->getSession()->get('userId'))) {
            $this->request->getSession()->set('loggedin', false) ;
        }
        $this->request->getSession()
            ->set('logo', ($this->orm->getRepository(HomeUser::class)
            ->findOneBy(['active'=>true]))
                ->getLogo()->getPath()) ;
        $this->request->getSession()
            ->set('siteName', ( $this->orm->getRepository(HomeUser::class)
                ->findOneBy(['active'=>true]))
                    ->getSiteName()) ;
//        $this->request->getSession()
//            ->set('home', $this->orm->getRepository(HomeUser::class)->findOneBy(['active'=>true])) ;
        return $this->request->getSession();
    }

    public function routes()
    {
        return yaml_parse_file(__DIR__ . '/../../config/routes.yml');
    }

    public function config()
    {
        return $this->config;
    }

    public function start()
    {
        return (
            new Router(
                $this->request->server->get('REQUEST_URI') ?? '',
                $this->request->server->get('REQUEST_METHOD'),
                $this->routes()
            )
        ) ->run();
    }
}
