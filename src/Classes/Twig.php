<?php


namespace Blog\Classes;

use Exception;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\HttpFoundation\Session\Storage\PhpBridgeSessionStorage;
use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;
use Twig\Extension\DebugExtension;
use Twig\Loader\FilesystemLoader;

class Twig
{
    protected FilesystemLoader $loader;
    protected Environment $twig;
    protected Request $request;

    public function __construct()
    {
        $this->request = new Request();
        $this->loader = new FilesystemLoader(__DIR__ .'/../../view', getcwd() . './../');
        $this->twig = new Environment($this->loader, array('debug' => true));
        $this->twig->addExtension(new DebugExtension());
    }

    public function addGlobal($name, $value)
    {
         $this->twig->addGlobal($name, $value);
    }

    public function renderView(string $view, ?array $params)
    {
        try {
            return $this->twig->render($view, $params);
        } catch (LoaderError $e) {
            return (new Response('Erreur de Template: '.$e->getMessage()))->send();
        } catch (RuntimeError $e) {
            return (new Response('Erreur: '.$e->getMessage()))->send();
        } catch (SyntaxError $e) {
            return (new Response('Erreur de Syntaxe: '.$e->getMessage()))->send();
        }
    }

    public function render(string $view, ?array $params)
    {
        try {
            return (new Response($this->twig->render($view, $params)))->send();
        } catch (LoaderError $e) {
            return (new Response('Erreur de Template: '.$e->getMessage()))->send();
        } catch (RuntimeError $e) {
            return (new Response('Erreur: '.$e->getMessage()))->send();
        } catch (SyntaxError $e) {
            return (new Response('Erreur de Syntaxe: '.$e->getMessage()))->send();
        }
    }
}
