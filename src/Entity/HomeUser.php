<?php


namespace Blog\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class HomeUser
 * @package Blog\Entity
 * @ORM\Table(name="home")
 * @ORM\Entity
 */
class HomeUser
{

    /**
     * @ORM\Column(name="id", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private int $homeId;

    /**
     * @ORM\Column(type="string", nullable=false, length=255)
     */
    private ?string $homeName;

    /**
     * @ORM\Column(type="boolean")
     */
    private bool $active;

    /**
     * @ORM\Column(type="string", nullable=true, length=255)
     */
    private ?string $firstName = null;
    /**
     * @ORM\Column(type="string", nullable=true, length=255)
     */
    private ?string $lastName = null;
    /**
     * @ORM\Column(type="string", nullable=true, length=255)
     */
    private ?string $email = null;
    /**
     * @ORM\Column(type="string", nullable=true, length=255)
     */
    private ?string $phone = null;
    /**
     * @ORM\Column(type="string", nullable=true, length=255)
     */
    private ?string $title = null;
    /**
     * @ORM\Column(type="string", nullable=true, length=255)
     */
    private ?string $linkedin = null;
    /**
     * @ORM\Column(type="string", nullable=true, length=255)
     */
    private ?string $twitter = null;
    /**
     * @ORM\Column(type="string", nullable=true, length=255)
     */
    private ?string $pdf = null;

    /**
     * @ORM\OneToOne(targetEntity=Image::class, cascade={"persist", "remove"})
     *
     */
    private ?Image $profilePic = null;

    /**
     * @ORM\Column(type="string", nullable=true, length=2000)
     */
    private ?string $about = null;
    /**
     * @ORM\Column(type="string", nullable=true, length=255)
     */
    private ?string $siteName = null;

    /**
     * @ORM\OneToOne(targetEntity=Image::class,cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=true)
     */
    private ?Image $logo = null;

    /**
     * @ORM\ManyToMany(targetEntity="Skill")
     * @ORM\JoinTable(name="home_skills",
     *      joinColumns={@ORM\JoinColumn(name="home_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="skill_id", referencedColumnName="id")}
     *      )
     * @ORM\OrderBy({"value"="DESC"})
     */
    private Collection $skills;

    public function __construct()
    {
        $this->skills = new ArrayCollection();
        $this->active = false;
    }

    public function hydrate(array $data)
    {
        foreach ($data as $key => $value) {
            $method = 'set'.ucfirst($key);
            if (method_exists($this, $method)) {
                $this->$method($value);
            }
        }
    }

    public function getId(): int
    {
        return $this->homeId;
    }

    public function getHomeName(): ?string
    {
        return $this->homeName;
    }

    public function setHomeName(?string $homeName): HomeUser
    {
        $this->homeName = $homeName;
        return $this;
    }

    public function isActive(): bool
    {
        return $this->active;
    }

    public function setActive(bool $active): HomeUser
    {
        $this->active = $active;
        return $this;
    }

    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    public function setFirstName(?string $firstName): HomeUser
    {
        $this->firstName = $firstName;
        return $this;
    }

    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    public function setLastName(?string $lastName): HomeUser
    {
        $this->lastName = $lastName;
        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(?string $email): HomeUser
    {
        $this->email = $email;
        return $this;
    }

    public function getPhone(): ?string
    {
        return $this->phone;
    }

    public function setPhone(?string $phone): HomeUser
    {
        $this->phone = $phone;
        return $this;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(?string $title): HomeUser
    {
        $this->title = $title;
        return $this;
    }

    public function getLinkedin(): ?string
    {
        return $this->linkedin;
    }

    public function setLinkedin(?string $linkedin): HomeUser
    {
        $this->linkedin = $linkedin;
        return $this;
    }

    public function getTwitter(): ?string
    {
        return $this->twitter;
    }

    public function setTwitter(?string $twitter): HomeUser
    {
        $this->twitter = $twitter;
        return $this;
    }

    public function getPdf(): ?string
    {
        return $this->pdf;
    }

    public function setPdf(?string $pdf): HomeUser
    {
        $this->pdf = $pdf;
        return $this;
    }

    public function getProfilePic(): ?Image
    {
        return $this->profilePic;
    }

    public function setProfilePic(?Image $profilePic): HomeUser
    {
        $this->profilePic = $profilePic;
        return $this;
    }

    public function getAbout(): ?string
    {
        return $this->about;
    }

    public function setAbout(?string $about): HomeUser
    {
        $this->about = $about;
        return $this;
    }

    public function getSiteName(): ?string
    {
        return $this->siteName;
    }

    public function setSiteName(?string $siteName = null): HomeUser
    {
        $this->siteName = $siteName;
        return $this;
    }

    public function getLogo(): ?Image
    {
        return $this->logo;
    }

    public function setLogo(?Image $logo): HomeUser
    {
        $this->logo = $logo;
        return $this;
    }

    public function getSkills(): Collection
    {
        return $this->skills;
    }

    public function addSkill(?Skill $skill): HomeUser
    {
        if (!$this->skills->contains($skill)) {
            $this->skills[] = $skill;
        }
        return $this;
    }

    public function removeSkill(Skill $skill): HomeUser
    {
        if ($this->skills->contains($skill)) {
            $this->skills->removeElement($skill) ;
        }
        return $this;
    }
}
