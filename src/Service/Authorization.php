<?php


namespace Blog\Service;

use Blog\Classes\Blog;
use Exception;

class Authorization extends Blog
{

    public function __construct()
    {
        parent::__construct();
    }

    public function checkAuthorized($url)
    {
        $regexAdmin= "#^/admin#i";
        $session = $this->request->getSession();

        if (preg_match($regexAdmin, $url)) {
            if ($session->get('loggedin') !==true) {
                throw new Exception("Connectez vous pour accéder à l'administration!", 403);
            }
            if ($session->get('userRole') !==1) {
                throw new Exception("Vous n'avez pas accès à cette page!", 403);
            }
        }
    }
}
