<?php


namespace Blog\Service;

use Blog\Classes\Config;
use Exception;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class Upload
{

    protected array $config;

    public function __construct()
    {
        $this->config = (new Config())
        ->siteConfig();
    }


    public function uploadFile($files)
    {
        $message="";
        $allowedExt = $this->config['upload']['ext'];
        $upload_dir = $this->config['upload']['dir'];
        $maxsize = (int)$this->config['upload']['max_size'];
        foreach ($files as $file) {
            if (!empty($file)) {
                /**
                 * @var UploadedFile $file
                 */
                $file_path = $upload_dir .$file->getClientOriginalName();
                    // Validate file extension
                if (!array_key_exists(strtolower($file->getClientOriginalExtension()), $allowedExt)) {
                    throw new Exception("Erreur: Merci de sélectionner un format valide.");
                }
                    // Validate file size
                if ($file->getSize() > $maxsize) {
                    throw new Exception("Erreur: Le fichier est trop lourd.");
                }
                    // Validate type of the file
                if (!in_array($file->getClientMimeType(), $allowedExt)) {
                    throw new Exception("Error: Il y a eu un problème. Please try again.");
                }
                    //Upload the file
                if (!$file->move($upload_dir, $file->getClientOriginalName())) {
                    throw new Exception("Le fichier n'a pas été chargé");
                }
                $file = new UploadedFile($file_path, $file->getClientOriginalName(), $file->getClientMimeType());
                $message= "Fichier chargé.";
                return array($file, $message);
            }
        }
        return array($files, $message);
    }
}
