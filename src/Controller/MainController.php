<?php


namespace Blog\Controller;

use Blog\Classes\Blog;
use Blog\Entity\HomeUser;
use Blog\Entity\Post;
use Blog\Entity\User;
use DateTime;
use Symfony\Component\HttpFoundation\Response;

class MainController extends Blog
{
    public function __construct()
    {
        parent::__construct();
    }

    public function getHome()
    {
        $home = $this->orm->getRepository(HomeUser::class)->findOneBy(['active'=>true]);
        $posts = $this->orm->getRepository(Post::class)->findBy(
            array('status'=>['Publié']),
            array('creationDateTime'=>'DESC'),
            6,
            0
        );
        $this->twig->render('front/home.html.twig', [
            'posts' => $posts,
            'home'=>$home
        ]);
    }

    public function getAdmin()
    {
        $session = $this->request->getSession();
        if (!empty($session->get('userId'))) {
            /**
             * @var User $user
             */
            $user =$this->orm->getRepository(User::class)->find($session->get('userId'));
            $posts = $this->orm->getRepository(Post::class)->findBy(
                array('status'=>['Brouillon','Publié','Archivé']),
                array('creationDateTime'=>'DESC')
            );
            $this->twig->render(
                'back/dashboard.html.twig',
                [
                    'user'=>$user,
                    'posts'=>$posts
                ]
            ) ;
        }
    }

    public function getLegals()
    {
        $this->twig->render('legals.html.twig', [null]);
    }

    public function sendContactMessage()
    {
        $objet = $this->request->get('subject');
        $subject = "Message MyBlog - ".$objet;
        $from =$this->request->get('email');
        $name = $this->request->get('name');
        $message = $this->request->get('message');
        $date = new DateTime();
        $mailto =  $this->sendMail->getAdminMail();
        $template ="/mails/mail.contact.html.twig";
        $param=array(
            'from'=>$from,
            'name'=>$name,
            'message'=>$message,
            'objet'=>$objet,
            'date'=>$date
        );
        if ($this->sendMail->sendEmail($subject, $from, $name, $mailto, $template, $param)) {
            return (new Response("Succès"))->send();
        } else {
            return (new Response("Votre message n'a pas pu être envoyé\n"))->send();
        }
    }
}
