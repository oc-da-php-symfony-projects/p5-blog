<?php


namespace Blog\Controller;

use Blog\Classes\Blog;
use Blog\Entity\HomeUser;
use Blog\Entity\Skill;
use Blog\Service\Upload;
use Exception;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;

class HomeUserController extends Blog
{

    public function __construct()
    {
        parent::__construct();
    }

    public function indexHome()
    {
        $homes = $this->orm->getRepository(HomeUser::class)->findAll();
        $home = $this->orm->getRepository(HomeUser::class)->findBy(['active'=>true]);
        $this->twig->render('back/home/index.html.twig', [
            'homes'=>$homes,
            'home'=>$home
        ]);
    }

    public function newHome()
    {
        $home = new HomeUser();
        $home->hydrate((array)$this->request->request->all());
        $this->orm->persist($home);
        $this->orm->flush();
        return (new RedirectResponse('/admin/home/edition/'.$home->getId()))->send() ;
    }

    public function getHomeEdit($homeId)
    {
        /**
         * @var HomeUser $home
         */
        $home = $this->orm->getRepository(HomeUser::class)->find($homeId[0]);
        $about = trim($home->getAbout());
        $delimiter ='#<p class="lead">#';
        $abouts = array_map([$this,'trimstring'], preg_split($delimiter, $about));
        array_shift($abouts);
        $this->twig->render('back/home/home.html.twig', ['home' => $home,'abouts'=>$abouts]);
    }

    public function trimstring($string)
    {
        return trim($string, '</p>');
    }

    public function activateHome()
    {
        $message="";
        $active = $this->orm->getRepository(HomeUser::class)->findBy(['active'=>true]);
        $home = $this->orm->getRepository(HomeUser::class)->find($this->request->get('homeId'));
        if ($this->request->get('active') === 'true') {
            foreach ($active as $item) {
                $item->setActive(false);
                $this->orm->persist($item);
            }
            $home->setActive(true);
            $message = '<p style="background-color: lightseagreen;padding: 1em;">Configuration activée</p>';
        } else {
            if (count($active)>1) {
                $home->setActive(false);
                $message = '<p style="background-color: lightseagreen;padding: 1em;">Configuration désactivée</p>';
            } else {
                $message = '<p style="background-color: lightpink;padding: 1em;">
                            Il faut garder au moins une configuration active
                            </p>';
            }
        }
        $this->orm->persist($home);
        $this->orm->flush();
        $this->request->getSession()->set('home',$home);
        $this->twig->render('back/home/home.activate.html.twig', ['home'=>$home, 'message'=>$message]);
    }

    public function editConfigName()
    {
        $home=$this->orm->getRepository(HomeUser::class)->find($this->request->get('homeid'));
        $homeName = $this->request->get('homeEditName');
        $home->setHomeName($homeName);
        $this->orm->persist($home);
        $this->orm->flush();
        return (new RedirectResponse('/admin/home/edition/'.$home->getId()))->send();
    }

    public function createSkill()
    {
        $skillName = $this->request->get('owner-skill-name');
        $skillValue = $this->request->get('owner-skill-value');
        $home = $this->orm->getRepository(HomeUser::class)->findOneBy(['homeId'=>$this->request->get('homeId')]);
        $skill = new Skill();
        $skill->setDefault($skillName, $skillValue);
        $this->orm->persist($skill);
        $this->orm->flush();
        $home->addSkill($skill);
        $this->orm->persist($home);
        $this->orm->flush();
        return (new RedirectResponse('/admin/home/edition/'.$this->request->get('homeId')))->send() ;
    }

    public function editSkill()
    {
        $skill = $this->orm->getRepository(Skill::class)->findOneBy(['skillId'=>$this->request->get('skillId')]);
        $skill->setName($this->request->get('skillName'));
        $skill->setValue($this->request->get('skillValue'));
        $this->orm->persist($skill);
        $this->orm->flush();
        $this->twig->render('back/home/skill.row.html.twig', ['skill'=>$skill]);
    }

    public function deleteSkill()
    {
        $home=$this->orm->getRepository(HomeUser::class)->find($this->request->get('homeId'));
        $skill=$this->orm->getRepository(Skill::class)->find($this->request->get('skillId'));
        $home->removeSkill($skill);
        $this->orm->persist($home);
        $this->orm->flush();
        $this->orm->remove($skill);
        $this->orm->flush();
        $this->twig->render('back/home/inside.owner.skills.html.twig', ['home'=>$home]);
    }

    public function editAbout($homeId)
    {
        $about="";
        $home=$this->orm->getRepository(HomeUser::class)->find($homeId[0]);
        $paragraphes = $this->request->request->all();
        foreach ($paragraphes as $paragraphe) {
            if ($paragraphe !="") {
                $about .= '<p class="lead">' . $paragraphe . '</p>';
            }
        }
        $home->setAbout($about);
        $this->orm->persist($home);
        $this->orm->flush();
        return (new RedirectResponse('/admin/home/edition/'.$home->getId()))->send() ;
    }

    public function editSite($home)
    {
        $home=$this->orm->getRepository(HomeUser::class)->find($home[0]);
        if (!empty($this->request->files)) {
            $upLoad= new Upload;
            $upLoad->uploadFile($this->request->files);
            $logo = $this->request->files->get('site-logo');
            $upload_dir =$this->config['upload']['dir'];
            $logo_path = $upload_dir . $logo->getClientOriginalName();
            $ImageController = new ImageController;
            $siteLogo = $ImageController->createImage($logo->getClientOriginalName(), $logo_path);
            $home->setLogo($siteLogo);
        }
        if (!empty($this->request->get('site-name'))) {
            $siteName = $this->request->get('site-name');
            $home->setSiteName($siteName);
        }
        $this->orm->persist($home);
        $this->orm->flush();
        return (new RedirectResponse('/admin/home/edition/'.$home->getId()))->send() ;
    }

    public function editSocial()
    {
        $home = $this->orm->getRepository(HomeUser::class)->find($this->request->get('home'));
        $data =$this->request->request->all();
        $home->hydrate((array)$data);
        $this->orm->persist($home);
        $this->orm->flush();
        return (new RedirectResponse('/admin/home/edition/'.$home->getId()))->send() ;
    }

    public function editHome()
    {
        $upLoad = new Upload();
        $imageController = new ImageController;
        $files = $this->request->files;
        $home = $this->orm->getRepository(HomeUser::class)->findOneBy(['homeId'=>$this->request->get('homeId')]);
        $img = $this->request->files->get("image");
        $pdf =$this->request->files->get("pdf");
        try {
            $upLoad->uploadFile($files);
        } catch (Exception $e) {
            return (new Response($e->getMessage()))->send();
        }
        $upload_dir =$this->config['upload']['dir'];
        if (!empty($img)) {
            $img_path = $upload_dir .$img->getClientOriginalName();
            $image = $imageController->createImage($img->getClientOriginalName(), $img_path);
            $home->setProfilePic($image);
        }
        if (!empty($pdf['name'])) {
            $pdf_path = $upload_dir . $pdf->getClientOriginalName();
            $home->setPdf($pdf_path);
        }
        $home->hydrate((array)$this->request->request->all());
        $this->orm->persist($home);
        $this->orm->flush();
        return (new RedirectResponse('/admin/home/edition/'.$this->request->get('homeId')))->send();
    }
}
