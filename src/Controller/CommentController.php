<?php


namespace Blog\Controller;

use Blog\Classes\Blog;
use Blog\Entity\Comment;
use Blog\Entity\Post;
use Blog\Entity\User;
use Symfony\Component\HttpFoundation\RedirectResponse;

class CommentController extends Blog
{
    public function __construct()
    {
        parent::__construct();
    }

    public function addComment(array $params)
    {
        $postId = $params[0];
        $commentId = $params[1];
        if ($this->session->get('loggedin') != true) {
            $this->session->set('url_requested', '/blog/post/'.$postId);
            return (new RedirectResponse('/connexion'))->send();
        }
        if (!empty($this->request->get('comment-content'))) {
            $content = $this->request->get('comment-content');
            /**
             * @var Post $post
             */
            $post =  $this->orm->getRepository(Post::class)->find($postId);
            /**
             * @var User $user
             */
            $user =  $this->orm->getRepository(User::class)->find($this->session->get('userId'));
            $comment = new Comment();
            $content =filter_var(
                trim($this->request->get('comment-content')),
                FILTER_SANITIZE_STRING,
                FILTER_FLAG_STRIP_LOW
            );
            $comment->setContent($content);
            $comment->setPost($post);
            $comment->setUser($user);
            if ($commentId !== "null") {
                /**
                 * @var Comment $parent
                 */
                $parent = $this->orm->getRepository(Comment::class)->find($commentId);
                $comment->setParent($parent);
            }
            $this->orm->persist($comment);
            $this->orm->flush();
            $post->addComment($comment);
            $user->addComment($comment);
            $this->orm->persist($post);
            $this->orm->flush();
            $this->orm->persist($user);
            $this->orm->flush();
        }
        return (new RedirectResponse('/blog/post/'.$postId))->send();
    }

    public function editCommentView($commentId)
    {
        /**
         * @var Comment $comment
         */
        $comment = $this->orm->getRepository(Comment::class)->find($commentId[0]);
        $user = $comment->getUser();
        if ($this->session['loggedin'] = true && isset($this->session['user']['id'])) {
            if ($this->session['user']['id']== $user->getId()) {
                $this->twig->render('front/comment.html.twig', ['user' => $user, 'comment' => $comment]);
            } else {
                $this->session['url_requested'] = '/blog/comment/edition/'.$commentId[0];
                return (new RedirectResponse('/connexion'))->send();
            }
        } else {
            $this->session['url_requested'] = '/blog/comment/edition/'.$commentId[0];
            return (new RedirectResponse('/connexion'))->send();
        }
    }

    public function editComment($commentId)
    {
        /**
         * @var Comment $comment
         */
        $comment = $this->orm->getRepository(Comment::class)->find($commentId[0]);
        if (!empty($this->request->get('comment-content'))) {
            $content = filter_var(
                trim(
                    $this->request->get('comment-content')
                ),
                FILTER_SANITIZE_STRING,
                FILTER_FLAG_STRIP_LOW
            );
            $comment->setContent($content);
            $comment->setStatus(Comment::STATUS_PENDING);
            $this->orm->persist($comment);
            $this->orm->flush();
            return (new RedirectResponse('/blog/post/'.$comment->getPost()->getId()))->send();
        }
    }

    public function commentsList()
    {
        $commentsList = $this->orm->getRepository(Comment::class)->findBy(
            array('status'=>['En Attente','Validé','Refusé']),
            array('dateTime'=>'DESC')
        );
        $this->twig->render('/back/comment/index.html.twig', ['commentsList'=>$commentsList]);
    }

    public function moderateComment()
    {
        $comment=null;
        if (!empty($this->request->get('status')) && !empty($this->request->get('commentId'))) {
            /**
             *  @var  Comment $comment
             */
            $comment = $this->orm->getRepository(Comment::class)->find($this->request->get('commentId'));
            $status = $this->request->get('status');
            switch ($status) {
                case 'En Attente':
                    $comment->setStatus(Comment::STATUS_PENDING);
                    break;
                case 'Validé':
                    $comment->setStatus(Comment::STATUS_VALID);
                    break;
                case 'Refusé':
                    $comment->setStatus(Comment::STATUS_DISMISS);
                    $template= '/mails/mail.editcomment.html.twig';
                    $params=array('comment'=>$comment);
                    $this->sendMail->sendEmail(
                        'Votre Commentaire sur MyBlog',
                        $this->sendMail->getAdminMail(),
                        'MyBlog - Modération',
                        $comment->getUser()->getEmail(),
                        $template,
                        $params
                    );
                    break;
                default:
                    $comment->setStatus(Comment::STATUS_PENDING);
            }
            $this->orm->persist($comment);
            $this->orm->flush();
        }
        $this->twig->render('back/comment/comment.status.html.twig', ['comment'=>$comment]);
    }

    public function deleteComment()
    {
        $comment=null;
        if (!empty($this->request->get('commentId'))) {
            /**
             *  @var  Comment $comment
             */
            $comment = $this->orm->getRepository(Comment::class)->find($this->request->get('commentId'));
            $comment->setStatus(Comment::STATUS_DELETED);
            $this->orm->persist($comment);
            $this->orm->flush();
        }
        $commentsList = $this->orm->getRepository(Comment::class)->findBy(
            array('status'=>['En Attente','Validé','Refusé']),
            array('dateTime'=>'DESC')
        );
        $this->twig->render('back/comment/inside.comments.list.html.twig', ['commentsList'=>$commentsList]);
    }
}
