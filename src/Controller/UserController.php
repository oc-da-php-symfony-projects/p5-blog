<?php


namespace Blog\Controller;

use Blog\Classes\Blog;
use Blog\Entity\User;
use Blog\Service\Upload;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;

class UserController extends Blog
{

    private ImageController $imageController;

    public function __construct()
    {
        parent::__construct();
        $this->imageController =  new ImageController();
    }

    public function indexUsers()
    {
        $users = $this->orm->getRepository(User::class)->findAll();
        $this->twig->render('back/users/index.html.twig', ['users'=>$users]);
    }

    public function manageUserStatus()
    {
        $active = $this->request->get('status');
        /**
         * @var User $user
         */
        $user = $this->orm->getRepository(User::class)->find($this->request->get('userId'));
        switch ($active) {
            case '0':
                $user->setActivated(false);
                break;
            case '1':
                $user->setActivated(true);
        }
        $this->orm->persist($user);
        $this->orm->flush();
        return $user;
    }

    public function getAccount()
    {
        if ($this->session->get('loggedin')== false) {
            $securityController = new SecurityController;
            $securityController->loginForm();
        } elseif ($this->session->get('loggedin') == true) {
            /**
             * @var User $user
             */
            $user =$this->orm->getRepository(User::class)->find($this->session->get('userId'));
            $this->twig->render('user/account.html.twig', ['user'=>$user]);
        }
        return;
    }

    public function getProfile($userId)
    {
            /**
             * @var User $user
             */
            $user =$this->orm->getRepository(User::class)->find($userId[0]);
        $this->twig->render('user/profile.html.twig', ['user'=>$user]);
    }

    public function addUserProfilePic()
    {
        if (!empty($this->request->get("submit"))) {
            /**
             * @var $user User
             */
            $user = $this->orm->getRepository(User::class)->find($this->session->get('userId'));
            $file = $this->request->files->get("userImage");
            $config = $this->config;
            $upload_dir = $config['upload']['dir'];
            $file_path = $upload_dir . $file->getClientOriginalName();
            $upLoad =  new Upload();
            if ($upLoad->uploadFile($this->request->files)) {
                $image = $this->imageController->createImage($file->getClientOriginalName(), $file_path);
                $image->setUser($user);
                $image->setHighlight(true);
                $this->orm->persist($image);
                $this->orm->flush();
                $user->addImage($image);
                $user->setProfilePic($image->getPath());
                $this->orm->persist($user);
                $this->orm->flush();
            }
        }
        return (new RedirectResponse('/user/account'))->send();
    }

    public function editUserInfos($userId)
    {
        $title = filter_var(trim($this->request->get('edit-title')), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
        $firstname = filter_var(
            trim($this->request->get('edit-firstname')),
            FILTER_SANITIZE_STRING,
            FILTER_FLAG_STRIP_LOW
        );
        $lastname = filter_var(
            trim($this->request->get('edit-lastname')),
            FILTER_SANITIZE_STRING,
            FILTER_FLAG_STRIP_LOW
        );
        $username = filter_var(
            trim($this->request->get('edit-username')),
            FILTER_SANITIZE_STRING,
            FILTER_FLAG_STRIP_LOW
        );
        $user = $this->orm->getRepository(User::class)->find($userId[0]);
        $user->setTitle($title);
        $user->setFirstName($firstname);
        $user->setLastName($lastname);
        $user->setUserName($username);
        $this->orm->persist($user);
        $this->orm->flush();
        return (new RedirectResponse('/user/account'))->send();
    }

    public function editUserPassword($userId)
    {
        /**
         * @var User $user
         */
        $user = $this->orm->getRepository(User::class)->find($userId[0]);
        $password =$user->getPassword();
        $newpass = filter_var(
            trim($this->request->get('new-user-pwd')),
            FILTER_SANITIZE_STRING,
            FILTER_FLAG_STRIP_LOW
        );
        $oldpass = filter_var(
            trim($this->request->get('former-user-pwd-pwd')),
            FILTER_SANITIZE_STRING,
            FILTER_FLAG_STRIP_LOW
        );
        if (($this->session->get('loggedin') == true)
            && ($this->session->get('userId') == $userId[0])
            && (password_verify($oldpass, $password))
        ) {
            $user->makePassword($newpass);
            $this->orm->persist($user);
            $this->orm->flush();
            $subject = 'Nouveau Mot de Passe sur MyBlog';
            $mailto = $user->getEmail();
            $from = $this->sendMail->getAdminMail();
            $name = 'My Blog';
            $template = '/mails/mail.newpwd.html.twig';
            $params = array('user'=>$user);
            $this->sendMail->sendEmail($subject, $from, $name, $mailto, $template, $params);
            return (new RedirectResponse('/deconnexion'))->send();
        } else {
            return (new Response('error'))->send();
        }
    }

    public function editUserEmail($userId)
    {
        /**
         * @var User $user
         */
        $user = $this->orm->getRepository(User::class)->find($userId[0]);
        $password =$user->getPassword();
        $newmail = filter_var(
            trim($this->request->get('new-user-email')),
            FILTER_VALIDATE_EMAIL,
            FILTER_FLAG_STRIP_LOW
        );
        $oldpass = $this->request->get('former-user-pwd-email');
        if (($this->session->get('loggedin') == true)
            && ($this->session->get('userid') == $userId[0])
            && (password_verify($oldpass, $password)
            && (!$this->orm->getRepository(User::class)->findOneBy(['email'=>$newmail])))
        ) {
            $user->setEmail($newmail);
            $this->orm->persist($user);
            $this->orm->flush();
            $subject = 'Votre nouvel Email sur MyBlog';
            $mailto = $user->getEmail();
            $from = $this->sendMail->getAdminMail();
            $name = 'My Blog';
            $template = '/mails/mail.newemail.html.twig';
            $params = array('user'=>$user);
            $this->sendMail->sendEmail($subject, $from, $name, $mailto, $template, $params);
            return (new RedirectResponse('/deconnexion'))->send();
        } else {
            return (new Response('error'))->send();
        }
    }

    public function deactivateAccount($userId){
        /**
         * @var User $user
         */
        $user = $this->orm->getRepository(User::class)->find($userId[0]);
        $user->setActivated(false);
        $this->orm->persist($user);
        $this->orm->flush();
        return (new RedirectResponse('/deconnexion'))->send();
    }

}
