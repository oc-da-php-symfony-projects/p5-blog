<?php


namespace Blog\Controller;

use Blog\Classes\Orm;
use Blog\Classes\Twig;
use Blog\Entity\Image;
use Blog\Orm\MyEntityManager;
use Intervention\Image\ImageManager;

class ImageController
{
    protected ImageManager $imageManager;
    private MyEntityManager $orm;
    private Twig $twig;

    public function __construct()
    {
        $this->orm = new MyEntityManager();
        $this->twig =  new Twig();
        $this->imageManager =new ImageManager();
    }

    public function createImage($name, $path)
    {
        $image = new Image();
        $image->setName($name);
        $image->setPath($path);
        $this->orm->persist($image);
        $this->orm->flush();

        return $image;
    }

    public function makeImage()
    {
        $image =  $this->imageManager->make('img/submarine.png')->resize(150, null, function ($constraint) {
            $constraint->aspectRatio();
        });
        $image->save('img/test/submarine.png');
    }
}
