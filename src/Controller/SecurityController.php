<?php


namespace Blog\Controller;

use Blog\Classes\Blog;
use Blog\Entity\Role;
use Blog\Entity\User;
use Exception;
use Symfony\Component\HttpFoundation\RedirectResponse;

class SecurityController extends Blog
{
    public function __construct()
    {
        parent::__construct();
    }

    public function Test()
    {
//        $subject = "Bienvenue sur MyBlog";
//        $from = 'dustmslf@gmx.fr';
//        $name = 'Admin';
//        $mailto = 'dustmslf@gmx.fr';
//        $template ="/mails/mail.register.html.twig";
//        $param=array('user'=>'koko');
//        if ($this->sendMail->sendEmail($subject, $from, $name, $mailto, $template, $param)) {
//            echo "Sent\n";
//        } else {
//            echo "Failed\n";
//        }
        $message =  $message = "<p>Votre inscription est bien prise en compte,
                    vérifiez votre boite mail nous vous avons envoyé un mail d'activation</p>
                     <p><a href='/blog/user/resend/activation/13'>Renvoyer le mail</a> </p>";
        $this->twig->render('success.html.twig', ['message'=>$message]);
    }



    public function loginForm()
    {

        if ($this->session->get('loggedin')== true) {
             (new RedirectResponse('/user/account'))->send();
        } else {
            $this->twig->render('form.login.html.twig', ['session' => $this->session]);
        }
    }

    public function registerForm()
    {
        $this->twig->render('form.register.html.twig', ['session' => $this->session]);
    }

    /**
     * @return string
     */
    public function registerAction()
    {
        $user = new User();
        try {
            if (empty($this->request->get('username')) ||
                empty($this->request->get('email')) ||
                empty($this->request->get('password'))) {
                throw new Exception("Merci de remplir tous les champs");
            }
            if (!filter_var(
                trim($this->request->get('email')),
                FILTER_VALIDATE_EMAIL,
                FILTER_FLAG_STRIP_LOW
            )) {
                throw new Exception("Merci d'indiquer une adresse mail valide");
            }
            if ($this->orm->getRepository(User::class)->findBy(['email'=>$this->request->get('email')])) {
                throw new Exception("Cette adresse mail est deja associée à un compte");
            }
            if ($this->orm->getRepository(User::class)->findBy(['userName'=>$this->request->get('username')])) {
                throw new Exception("Ce nom d'utilisateur est deja pris");
            }
            $username =  filter_var(
                trim($this->request->get('username')),
                FILTER_SANITIZE_STRING,
                FILTER_FLAG_STRIP_LOW
            );
            $email = filter_var(
                trim($this->request->get('email')),
                FILTER_VALIDATE_EMAIL,
                FILTER_FLAG_STRIP_LOW
            );
            $password = filter_var(
                trim($this->request->get('password')),
                FILTER_SANITIZE_STRING,
                FILTER_FLAG_STRIP_LOW
            );
            /**
             * @var Role $role
             */
            $role = $this->orm->getRepository(Role::class)->find(3);
            $user->setDefault($username, $email, $role);
            $user->makePassword($password);
            $this->orm->persist($user);
            $this->orm->flush();
            $subject = "Bienvenue sur MyBlog";
            $from =  $this->sendMail->getAdminMail();
            $name = 'Admin';
            $mailto = $email;
            $template ="/mails/mail.register.html.twig";
            $param=array('user'=>$user);
            if (!$this->sendMail->sendEmail($subject, $from, $name, $mailto, $template, $param)) {
                throw new Exception(
                    "<p>Votre inscription est bien prise en compte,
                     mais une erreur inconnue nous a empéché de vous envoyer le mail d'activation</p>
                     <p><a href='/blog/user/resend/activation/".$user->getId()."'>Renvoyer le mail</a> </p>"
                );
            } else {
                    $message = "<p>Votre inscription est bien prise en compte,
                            vérifiez votre boite mail nous vous avons envoyé un mail d'activation</p>
                             <p><a href='/blog/user/resend/activation/".$user->getId()."'>Renvoyer le mail</a> </p>";
                    $this->twig->render('success.html.twig', ['message'=>$message]);
            }
        } catch (Exception $e) {
            $this->twig->render('form.register.html.twig', ['exception'=>$e]);
        }
    }

    public function resendActivationEmail($userId)
    {
        $user = $this->orm->getRepository(User::class)->find($userId[0]);
        $subject = "Bienvenue sur MyBlog";
        $from =  $this->sendMail->getAdminMail();
        $name = 'Admin';
        $mailto = $user->getEmail();
        $template ="/mails/mail.register.html.twig";
        $param=array('user'=>$user);
        $this->sendMail->sendEmail($subject, $from, $name, $mailto, $template, $param);
        $message = "<p>Vérifiez votre boite mail nous vous avons renvoyé un mail d'activation</p>
                    <p><a href='/blog/user/resend/activation/".$user->getId()."'>Renvoyer le mail</a> </p>";
        $this->twig->render('success.html.twig', ['message'=>$message]);
        return (new RedirectResponse('/connexion'))->send();
    }

    public function mailValidationAction($userId)
    {
        try {
            /**
             * @var User $user
             */
            if (!$user = $this->orm->getRepository(User::class)->find($userId[0])) {
                throw new Exception("Nous n'avons pas pu trouver votre compte");
            }
            if (!$user->setEmailValidated(true)) {
                throw new Exception("Nous n'avons pas pu valider votre email");
            }
            if (!$user->setActivated(true)) {
                throw new Exception("Nous n'avons pas pu activer votre compte");
            }
            $this->orm->persist($user);
            $this->orm->flush();
            $message = 'Votre adresse mail est désormais validée!';
            $this->twig->render('/success.html.twig', ['message'=>$message]);
            $this->session->set('url_requested', '/user/account') ;
            return (new RedirectResponse('/connexion'))->send();
        } catch (Exception $e) {
            $this->twig->render('error.html.twig', ['exception'=>$e]);
        }
    }

    public function loginAction()
    {
        $exception = null;
        try {
            if (empty($this->request->get('email')) && empty($this->request->get('password'))) {
                throw new Exception("Merci de renseigner les champs");
            }
            $email =filter_var(
                trim($this->request->get('email')),
                FILTER_VALIDATE_EMAIL,
                FILTER_FLAG_STRIP_LOW
            );
        /**
         * @var  User $user
         */
            if(!$user =  $this->orm->getRepository(User::class)->findOneBy(['email'=>$email])) {
                throw new Exception("Utilisateur non trouvé");
            }
            $password =$user->getPassword();
            if (!password_verify($this->request->get('password'), $password)) {
                throw new Exception("Identifiant ou mot de passe erroné");
            }
            if (!$user->isEmailValidated()) {
                $message = "<p>Merci de valider votre e-mail pour accéder à votre compte</p>
                    <p><a href='/blog/user/resend/activation/".$user->getId()."'>Renvoyer le mail</a> </p>";
                throw new Exception($message);
            }
            if (!$user->isActivated()) {
                $message = "<p>Votre Compte est désactivé.</p>
                    <p><a href='/#contact'> Contactez nous pour le réactiver</a> </p>";
                throw new Exception($message);
            }
            $this->session->set('loggedin', true) ;
            $this->session->set('userId', $user->getId());
            $this->session->set('userUsername', $user->getUserName());
            $this->session->set('userProfilePic', $user->getProfilePic());
            $this->session->set('userRole', $user->getRole()->getId());
            if (!empty($this->session->get('url_requested'))) {
                $redirect =$this->session->get('url_requested');
                $this->session->set('url_requested', '');
                return (new RedirectResponse(''.$redirect))->send();
            } else {
                if ($user->getRole()->getId() == 1) {
                    return (new RedirectResponse('/admin'))->send();
                } else {
                    return (new RedirectResponse('/user/account'))->send();
                }
            }
        } catch (Exception $exception) {
             $this->twig->render('form.login.html.twig', ['exception'=>$exception]);
        }
    }

    public function logout()
    {
        $this->session->invalidate();
        return (new RedirectResponse('/connexion'))->send();
    }
}
