<?php


namespace Blog\Controller;

use Blog\Classes\Blog;
use Blog\Entity\Comment;
use Blog\Entity\Image;
use Blog\Entity\Post;
use Blog\Entity\User;
use Blog\Service\Upload;
use DateTime;
use Symfony\Component\HttpFoundation\RedirectResponse;

class PostController extends Blog
{

    public function __construct()
    {
        parent::__construct();
    }

    public function postDashboard()
    {
        $posts = $this->orm->getRepository(Post::class)->findBy(
            array('status'=>['Brouillon','Publié','Archivé']),
            array('creationDateTime'=>'DESC')
        );
        $this->twig->render('back/post/index.html.twig', ['posts'=>$posts]);
    }

    public function createPostView()
    {
        $this->twig->render('back/post/post.html.twig', []);
    }

    /**
     * @todo hydrate
     */
    public function createPost()
    {
        /**
         * @var User $user
         */
        $user=$this->orm->getRepository(User::class)->findOneBy(['userId'=>1]);
        $upload_dir = $this->config['upload']['dir'];
        $post= new Post();
        $image=null;
        if (!empty($this->request->files->get('post-image'))) {
            $upload = new Upload();
            if ($upload->uploadFile($this->request->files)) {
                $file = $this->request->files->get('post-image');
                $file_path = $upload_dir . $file->getClientOriginalName();
                $imageController = new ImageController;
                $image = $imageController->createImage($file->getClientOriginalName(), $file_path);

            }
        }
            $title = filter_var(
                trim(
                    $this->request->get('post-title')
                ),
                FILTER_SANITIZE_STRING,
                FILTER_FLAG_STRIP_LOW
            );
            $chapo = filter_var(
                trim(
                    $this->request->get('post-chapo')
                ),
                FILTER_SANITIZE_STRING,
                FILTER_FLAG_STRIP_LOW
            );
            $post->setTitle($title);
            $post->setChapo($chapo);
            $post->setContent($this->request->get('content'));
            $post->setUser($user);
            $this->orm->persist($post);
            $this->orm->flush();

        if ($image) {
            $image->setPost($post);
            $image->setHighlight(true);
            $this->orm->persist($image);
            $this->orm->flush();
            $post->addImage($image);
        }
                $user->addPost($post);
                $this->orm->persist($user);
                $this->orm->persist($post);
                $this->orm->flush();

        return (new RedirectResponse('/admin/posts/edition/'.$post->getId()))->send();
    }

    public function manageStatus()
    {
        $post=null;
        if (!empty($this->request->get('status')) && !empty($this->request->get('postId'))) {
            $post = $this->orm->getRepository(Post::class)->find($this->request->get('postId'));
            $status = $this->request->get('status');
            switch ($status) {
                case 'Brouillon':
                    $post->setStatus(Post::STATUS_DRAFT);
                    break;
                case 'Publié':
                    $post->setStatus(Post::STATUS_PUBLISHED);
                    break;
                case 'Archivé':
                    $post->setStatus(Post::STATUS_ARCHIVE);
                    break;
                default:
                    $post->setStatus(Post::STATUS_DRAFT);
            }
            $this->orm->persist($post);
            $this->orm->flush();
        }
        $this->twig->render('back/post/post.status.html.twig', ['post'=>$post]);
    }

    public function postEditView($postId)
    {
        $post = $this->orm->getRepository(Post::class)->find($postId[0]);
        $this->twig->render('back/post/include.post.edit.html.twig', ['post'=>$post]);
    }

    public function postEdit($postId)
    {
        $post = $this->orm->getRepository(Post::class)->find($postId[0]);
        if (!empty($this->request->request)) {
            $post->hydrate((array)$this->request->request->all());
            $post->setLastUpdateDateTime(new DateTime());
            $this->orm->persist($post);
            $this->orm->flush();

            return (new RedirectResponse('/admin/posts/edition/'.$postId[0]))->send();
        }
    }

    public function postEditImg($postId)
    {

        /**
         * @var $post Post
         */
        $post = $this->orm->getRepository(Post::class)->find($postId[0]);
        if (!empty($this->request->files)) {
            $file = $this->request->files->get('edit-post-image');
            $upload_dir = $this->config['upload']['dir'];
            $file_path = $upload_dir .$file->getClientOriginalName();
            $upLoad = new Upload();
            try {
                if ($upLoad->uploadFile($this->request->files)) {
                    $imageController =new ImageController();
                    $image = $imageController->createImage($file->getClientOriginalName(), $file_path);
                    $image->setPost($post);
                    $postImg = $post->getImages();
                    foreach ($postImg as $pImg) {
                        /**
                         * @var Image $pImg
                         */
                        if ($pImg->isHighlight()) {
                            $pImg->setHighlight(false);
                            $this->orm->persist($pImg);
                            $this->orm->flush();
                        }
                    }
                    $image->setHighlight(true);
                    $this->orm->persist($image);
                    $this->orm->flush();
                    $post->addImage($image);
                    $post->setLastUpdateDateTime(new DateTime());
                    $this->orm->persist($post);
                }
            } catch (\Exception $e) {
                return $e->getMessage();
            }
        }
        return (new RedirectResponse('/admin/posts/edition/'.$postId[0]))->send();
    }

    public function deletePost()
    {
        if (!empty($this->request->get('postId'))) {
            /**
             * @var Post $post
             */
            $post = $this->orm->getRepository(Post::class)->find($this->request->get('postId'));
            foreach ($post->getComments() as $comment) {
                /**
                 * @var Comment $comment
                 */
                $comment->setStatus(Comment::STATUS_DELETED);
                $this->orm->persist($comment);
                $this->orm->flush();
            }
            $post->setStatus(Post::STATUS_DELETED);
            $this->orm->persist($post);
            $this->orm->flush();
        }
        $posts = $this->orm->getRepository(Post::class)->findBy(
            array('status'=>['Brouillon','Publié','Archivé']),
            array('creationDateTime'=>'DESC')
        );
        $this->twig->render('back/post/inside.posts.list.html.twig', ['posts'=>$posts]);
    }

    public function removePost()
    {
        if (!empty($this->request->get('postId'))) {
            /**
             * @var Post $post
             */
            $post = $this->orm->getRepository(Post::class)->find($this->request->get('postId'));
            $this->orm->remove($post);
            $this->orm->flush();
        }
        $posts = $this->orm->getRepository(Post::class)->findBy(
            array('status'=>['Supprimé']),
            array('creationDateTime'=>'DESC')
        );
        $this->twig->render('back/post/inside.posts.list.html.twig', ['posts'=>$posts]);
    }

    public function getPostsList()
    {
        $posts = $this->orm->getRepository(Post::class)->findBy(
            array('status'=>['Publié']),
            array('creationDateTime'=>'DESC')
        );
        $this->twig->render('front/blog.html.twig', ['posts' => $posts]);
    }

    public function getCorbeille()
    {
        $posts = $this->orm->getRepository(Post::class)->findBy(
            array('status'=>['Supprimé']),
            array('creationDateTime'=>'DESC')
        );
        $this->twig->render('back/post/corbeille.html.twig', ['posts' => $posts]);
    }


    public function getPost($postId)
    {
        $message = "";
        $posts = $this->orm->getRepository(Post::class)->findBy(
            array('status'=>['Publié']),
            array('creationDateTime'=>'DESC')
        );
        /**
         * @var Post $post
         */
        if(empty($post = $this->orm->getRepository(Post::class)
            ->findOneBy(['postId'=>$postId[0],'status'=>'Publié']))) {
            $message = "Cet article n'existe plus ou a été déplacé";
            $comments = null;
        }
        $comments = $this->orm->getRepository(Comment::class)->findBy(['post'=>$post,'status'=>'Validé']);
        $this->twig->render('front/post.html.twig', [
            'posts' => $posts,
            'post' => $post,
            'comments' => $comments,
            'message' => $message
        ]);
    }
}
