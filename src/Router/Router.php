<?php

namespace Blog\Router;

use Blog\Classes\Twig;
use Blog\Service\Authorization;
use Exception;

class Router
{

    private ?string $url;
    private array $routes = [];
    private array $nammmedRoutes = [];
    private string $method;

    //Construction du routeur et des routes en ajoutant les routes parsées en fonction de la methode
    public function __construct(?string $url, string $method, array $routes)
    {
        $this->method = $method;
        $this->url = $url;
        $this->parse($routes);
    }

    public function parse(array $routes)
    {
        foreach ($routes as $name => $route) {
            $path = $route['path'];
            $callable = $route['callable'];
            if (isset($route['method']) && ($route['method'] == 'POST')) {
                $this->post($path, $callable, $name);
            } else {
                $this->get($path, $callable, $name);
            }
        }
    }

    public function get($path, $callable, ?string $name)
    {
        return $this->add($path, $callable, $name, 'GET');
    }

    public function post($path, $callable, ?string $name)
    {
        return $this->add($path, $callable, $name, 'POST');
    }

    private function add($path, $callable, $name, $method)
    {
        $route = new Route($path, $callable);
        $this->routes[$method][] = $route;
        if (is_string($callable) && $name === null) {
            $name = $callable;
        }
        if ($name) {
            $this->nammmedRoutes[$name] = $route;
        }
        return $route;
    }

    public function run()
    {
        try {
            if (!isset($this->routes[$this->method])) {
                throw new Exception('La Méthode n\'existe pas');
            }
            foreach ($this->routes[$this->method] as $route) {
                if ($route->match($this->url)) {
                    $authorization = new Authorization();
                    $authorization->checkAuthorized($this->url);
                    return $route->call();
                }
            }
            throw new Exception('Cette page n\'existe pas', 404);
        } catch (Exception $exception) {
            $twig = new Twig;
            if ($exception->getCode()==403) {
                $twig->render('403.html.twig', ['exception' => $exception]);
            } else {
                $twig->render('error.html.twig', ['exception' => $exception]);
            }
        }
    }
}
