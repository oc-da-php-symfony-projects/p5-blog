<?php


namespace Blog\Router;

class Route
{


    private string $path;
    private string $callable;
    private array $matches = [];


    public function __construct(string $path, string $callable)
    {
        $this->path = trim($path, '/');
        $this->callable = $callable;
    }

    public function match($url)
    {
        $url = trim($url, '/');
        $path = preg_replace_callback('#:([\w]+)#', [$this,'paramMatch'], $this->path);
        $regex = "#^$path$#i";
        if (!preg_match($regex, $url, $matches)) {
            return false;
        }
        array_shift($matches);
        $this->matches = $matches;
        return true;
    }

    private function paramMatch()
    {
        return '([^/]+)';
    }


    public function call()
    {
        if (is_string($this->callable)) {
            $call = explode('#', $this->callable);
            $controller = "Blog\\Controller\\".$call[0].'Controller' ;
            $action = $call[1];
            $controller = new $controller();
            return $controller->$action($this->matches);
        }
    }

}
