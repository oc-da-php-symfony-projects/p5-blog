# P5 Blog

A simple Php blog made from scratch using some of the famous [Symfony](https://symfony.com/) framework's dependencies:

*  Doctrine
*  Twig
*  HttpFoundation

It also uses [Bootstrap 4 framework](https://getbootstrap.com/) 

## Prerequisite

For install, you will need (presuming this is a local Windows 10 install):

*  [Wamp](https://www.wampserver.com/) (if Debian/Ubuntu you will need [Apache](https://httpd.apache.org/), [mariaDB](https://mariadb.org/) and [phpMyAdmin](https://www.phpmyadmin.net/)
*  [Composer](https://getcomposer.org/).
*  Make sur you have .yml extension installed and enabled in your server. 

If you don't you can download it [here](http://pecl.php.net/package/yaml)

And follow instruction [here down in comments](https://stackoverflow.com/questions/27122701/installing-php-yaml-extension-with-xampp-on-windows)

## Installation

*   Download zip or clone repository from [Gitlab](https://gitlab.com/oc-da-php-symfony-projects/p5-blog)
*   Run composer install to get the dependencies
*   Create database with phpMyadmin
*   Enter your database configuration option in /config/orm.yml
*   Run php vendor/bin/doctrine orm:schema-tool:update --force in terminal
*   Enter smtp configuration option in /config/parameter.yml
*   You will need to manually update url roots in view/mails/
*   go to your-url.com/enregistrement
*   Create an account and activate it, then with phpMyadmin, go to table 'user' and set 'role' to 1 for your account, so you can administrate the site.
*   Go to your-url.com/admin/home/config and start editing your home page.

[Demo Here](http://myblog.ananzebatanga.com/)

Here are my Codacy Badge and SymfonyInsight Medal.

[![Codacy Badge](https://app.codacy.com/project/badge/Grade/2be7f44e35e54b78ba8427956801fd92)](https://www.codacy.com/gl/oc-da-php-symfony-projects/p5-blog?utm_source=gitlab.com&amp;utm_medium=referral&amp;utm_content=oc-da-php-symfony-projects/p5-blog&amp;utm_campaign=Badge_Grade)

[![SymfonyInsight](https://insight.symfony.com/projects/8f6e2bad-77ad-4ff8-89cd-2578cf9949d3/small.svg)](https://insight.symfony.com/projects/8f6e2bad-77ad-4ff8-89cd-2578cf9949d3)

This project is a scholar project.

Feel free to use or improve it! I would be honoured!

Let me tell you, I'm very happy of this, even I humbly know it is just a newbie stuff! 

Yes, I'm a beginner and so glad to be able of that. 

Thank you [OpenClassrooms](https://openclassrooms.com/fr/) for giving anybody anywhere some progress opportunities, thank you my Mentor Thomas Gérault for your kind but demanding leading and thank you my Boss at work for letting me go back to school! 

Let me learn more! 

Love it!
