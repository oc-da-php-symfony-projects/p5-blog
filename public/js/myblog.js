(function ($) {
    $('.file-img').change(function () {
        readUrl(this,'img');
        let fileName = $(this).val().split('\\').pop();
        $(this).next('.img-file-label').addClass("selected").html(fileName);
    });

    $('.file-pdf').on('change', function () {
        readUrl(this,'pdf');
        let fileName = $(this).val().split('\\').pop();
        $(this).next('.pdf-file-label').addClass("selected").html(fileName);
    });

    function readUrl(input,type)
    {
        if (input.files && input.files[0]) {
            let reader = new FileReader();
            reader.onload = function (e) {
                if (type === 'img') {
                    $('.fileimg').attr('src', e.target.result);
                } else {
                    $('.filepdf').attr('src', e.target.result);
                }
            }
            reader.readAsDataURL(input.files[0]);
        }
    }

})(jQuery);

// CookieConsent

/**
 * Init Cookie Consent Bar
 */
function initCookieConsentBar()
{
    bar = document.getElementsByClassName('cookie-consent')[0];
    button = document.getElementsByClassName('cookie-accept')[0];

    setEventListeners();
    fadeIn(bar, 250);
}

/**
 * Set button actions
 * @return null
 */
function setEventListeners()
{
    button.addEventListener('click', function () {
        $.cookie('cookie-consent', '1', 5*60*1000)
        fadeOut(bar, 250);
    });
}
// function showACForm(id) {
//     console.log(id);
//     id = 'answerComment'+id;
//     console.log(id);
//     let answer = document.getElementById(id);
//     console.log(answer);
//     // $('#answerComment'+id).append("<form class='form-mf' action='{{"/blog/post/repondre/commentaire/"~post.id~"/"}}"+id+"' method='post'>"+
//     answer.append("<form class='form-mf' action='{{"/blog/post/repondre/commentaire/"~post.id~"/"}}"+id+"' method='post'>"+
//         "<div class='row'>"+
//         "<div class='col-md-12 mb-3'>"+
//         "<div class='form-group'>"+
//         "<label for='comment-content'>Entrez votre réponse</label><br />"+
//         " <textarea id='comment-content' class='form-control input-mf' name='comment-content'  cols='45' rows='8' required></textarea>" +
//         "</div>"+
//         "</div>"+
//         "<div class='col-md-12'>"+
//         "<input type='submit' class='button button-a button-big button-rouded' value='Envoyer'>"+
//         "</div>"+
//         "</div>"+
//         "</form>"+
//         "</div>");
// }
/**
 * FadeIn effect
 * @param element - DOM Element
 * @param speed - effect duration
 * @return null
 */
function fadeIn(element, speed)
{
    var elementStyle = element.style;
    elementStyle.opacity = 0;
    elementStyle.display = 'block';
    (function fade()
    {
        (elementStyle.opacity -= -0.1) > 0.9 ? null : setTimeout(fade, (speed / 10));
    })();
}

/**
 * FadeOut effect
 * @param element - DOM Element
 * @param speed - effect duration
 * @return null
 */
function fadeOut(element, speed)
{
    var elementStyle = element.style;
    elementStyle.opacity = 1;
    (function fade()
    {
        (elementStyle.opacity -= 0.1) < 0.1 ? elementStyle.display = 'none' : setTimeout(fade, (speed / 10));
    })();
}

document.addEventListener('DOMContentLoaded', function () {
    var currentCookieSelection = $.cookie('cookie-consent');

    // Stop further execution, if the user already allowed cookie usage.
    if (currentCookieSelection !== null && currentCookieSelection !== undefined) {
        return;
    } else {
        initCookieConsentBar();
    }
});


// Fix: Allow to exist with or without jQuery
if ($ === null || typeof $ !== 'object') {
    var $ = new Object;
}

// Source: https://gist.githubusercontent.com/bronson/6707533/raw/7317b0e0d204d00d3b01d06f9f18a09ae4ee6f4e/cookie.js

// cookie.js
//
// Usage:
//  $.cookie('mine', 'data', 5*60*1000)  -- write data to cookie named mine that lasts for five minutes
//  $.cookie('mine')                     -- read the cookie that was just set, function result will be 'data'
//  $.cookie('mine', '', -1)             -- delete the cookie

$.cookie = function (name,value,ms) {
    if (arguments.length < 2) {
        // read cookie
        var cookies = document.cookie.split(';')
        for (var i=0; i < cookies.length; i++) {
            var c = cookies[i].replace(/^\s+/, '')
            if (c.indexOf(name+'=') === 0) {
                return decodeURIComponent(c.substring(name.length+1).split('+').join(' '))
            }
        }
        return null
    }

    // write cookie
    var date = new Date()
    date.setTime(date.getTime()+ms)
    document.cookie = name+"=" + encodeURIComponent(value) + (ms ? ";expires="+date.toGMTString() : '') + ";path=/"
}

document.addEventListener('DOMContentLoaded', function () {
    var currentCookieSelection = $.cookie('cookie-consent');

    // enable tracker code ony if cookie consent was given previously
    if (currentCookieSelection !== null && currentCookieSelection >= 1) {
        // activate tracking, e.g. Matomo
        // console.log('tracking allowed')
        //Pour aller plus loin dans l'utilisation du cookieconsent
    }
});