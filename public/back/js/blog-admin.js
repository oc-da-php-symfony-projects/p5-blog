(function ($) {
    $('.file-img').change(function () {
        let id = '-'+$(this).attr('id');
        readUrl(this,'img',id);
        let fileName = $(this).val().split('\\').pop();
        $(this).next('.img-file-label').addClass("selected").html(fileName);
    });

    $('.file-pdf').on('change', function () {
        readUrl(this,'pdf');
        let fileName = $(this).val().split('\\').pop();
        $(this).next('.pdf-file-label').addClass("selected").html(fileName);
    });

    function readUrl(input,type,id)
    {
        if (input.files && input.files[0]) {
            let reader = new FileReader();
            reader.onload = function (e) {
                if (type === 'img') {
                    $('.fileimg'+id).attr('src', e.target.result);
                } else {
                    $('.filepdf').attr('src', e.target.result);
                }
            }
            reader.readAsDataURL(input.files[0]);
        }
    }

    $('.siteActiveOption').on('change',function () {
        let active = $("input[name='siteActiveOption']:checked").val() ;
        let homeId = $('#idH').val();
        $.ajax(
            {
                url:'/admin/home/activate',
                method: 'POST',
                data: {
                    'active': active,
                    'homeId': homeId
                },
                success: function (activationData) {
                    $('.activate-home').html(activationData);
                    location.reload();
                }
            }
        );
    });

    let i =  $('.add-p').data("id");

    $('.add-p').click(function () {
        // console.log('old i: '+i);
        i++;
        // console.log('new i: '+i);
        $('.last').before(
            '<div id="paragraph-'+i+'" class="input-group ">\n' +
            '<div class="input-group-prepend">\n' +
            '<button type="button" id="delete-p" class=" delete-p custom-file-control btn btn-primary" data-value="'+i+'">Supprimer</button>\n' +
            '</div>\n' +
            '<textarea class="form-control col" id="about-par-'+i+'" name="about-par-'+i+'" placeholder="Paragraphe '+i+'"></textarea>\n' +
            '</div>'
        )
    });

    $(".about-par").on("click", ".delete-p", function () {
        let id = $(this).attr('data-value');
        $("#paragraph-"+id).remove();
    });

})(jQuery);


